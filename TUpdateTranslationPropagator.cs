﻿using UnityEngine;
using System.Collections;

public static class TUpdateTranslationPropagator {

	public static void PropagateForceUpdateTranslation (GameObject tar) {	//find every TranslatableUIText in the target tree and forward a ForceUpdateTranslation method call on each
		TranslatableUIText[] targets;
		targets = tar.GetComponentsInChildren<TranslatableUIText>(true);
		for (int i = 0; i < targets.Length; i++) {
			targets[i].ForceUpdateTranslation();
		}
	}

	public static void PropagateForceTranslate (GameObject tar) {	//find every TranslatableUIText in the target tree and forward a ForceTranslate method call on each
		TranslatableUIText[] targets;
		targets = tar.GetComponentsInChildren<TranslatableUIText>(true);
		for (int i = 0; i < targets.Length; i++) {
			targets[i].ForceTranslate();
		}
	}
}
