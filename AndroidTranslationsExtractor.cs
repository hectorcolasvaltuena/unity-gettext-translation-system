﻿using UnityEngine;
using System.Collections;
using System.IO;

public class AndroidTranslationsExtractor : MonoBehaviour {
	public static string[] languages = {	//arbitrarily fill this with the languages you're using
		"EN",
		"ES"
	};

	public static bool done = false;

	
	void Start () {
		#if UNITY_ANDROID
			Debug.Log("Beginning extraction of translation files...");
			StartCoroutine("ExtractTranslations");
		#else
			done = true;
		#endif
	}

	#if UNITY_ANDROID
		IEnumerator ExtractTranslations () {
			WWW www;
			string originPath;
			string targetPath;
			string fileName;

			for (int i = 0; i < languages.Length; i++) {
				originPath = Application.streamingAssetsPath + "/Translations/" + languages[i] +"/LC_MESSAGES";
				targetPath = Application.persistentDataPath + "/Translations/" + languages[i] +"/LC_MESSAGES";
				fileName = languages[i] + ".mo";

				Debug.Log("Extracting language file: " + fileName);

				//Check the existence of the target folder, otherwise create it
				if (!Directory.Exists(targetPath)) {
					Directory.CreateDirectory(targetPath);
				}

				www = new WWW(originPath + "/" + fileName);	//first use WWW class to open the .mo file packed in the jar
				yield return www;		//wait for WWW to finish reading the target file

				if (!string.IsNullOrEmpty(www.error)) {	//if there is an error log a warning
					Debug.LogError("Extraction of file " + fileName + " failed. \n" + www.error);
				}
				else {
					File.WriteAllBytes(targetPath + "/" + fileName, www.bytes);	//if successfully opened the packed .mo, write it to persistentDataPath
				}
			}
			Debug.Log("Extraction of language files finished");
			done = true;
			//T.SetLanguage(T.defaultLanguage);	//once loaded, initialize the catalog to a default language
		}
	#endif
}
