using System;
using NGettext;
using UnityEngine;
using System.Globalization;

//
// Usage:
//     T._("Hello, World!");
//     T._n("You have {0} apple.", "You have {0} apples.", count, count);
//
// Translation .mo files must be placed inside Assets/StreamingAssets/Translations/##/LC_MESSAGES (## being the ISO 639-1 language code)
//

public static class T {
	public static string defaultLanguage = "--";
	public static string curLanguage = defaultLanguage;

	//private static readonly string translationsPath = Application.streamingAssetsPath + "/Translations";	//Application.dataPath + "/Translations";
	private static ICatalog _Catalog = null;	//under android, initialize the catalog to _null as the translation files are not yet accessible
		/*#if UNITY_ANDROID
			null;
		#else
			new Catalog(defaultLanguage, GetTranslationsPath(), CultureInfo.CreateSpecificCulture(defaultLanguage));	
		#endif*/

	/*
	#if UNITY_ANDROID
		private static bool catalogsUnpacked = false;
	#endif
	*/

	public static void SetLanguage (string tar) {
		Debug.Log("Trying to set language: " + tar);
		#if UNITY_ANDROID	//unity android requires unzipping the language files beforehand
			if (!AndroidTranslationsExtractor.done) {
				Debug.LogError("Trying to access language catalogs before unpacking. Under Android, please use AndroidTranslationsExtractor to extract catalogs to a usable file");
				//return;
				//UnpackCatalogs();
			}
		#endif

		_Catalog = new Catalog(tar, GetTranslationsPath(), CultureInfo.CreateSpecificCulture(tar));
		curLanguage = tar;
	}

	public static string GetTranslationsPath () {
		#if UNITY_ANDROID
			return Application.persistentDataPath + "/Translations";
		#else
			return Application.streamingAssetsPath + "/Translations";
		#endif
	}

/*
	#if UNITY_ANDROID	//only necessary in android, this procedure extracts the .po files from the .jar file and places them somewhere readable.
		public static void UnpackCatalogs () {	
			Debug.Log("Unpacking language catalogs...");
			for (int i = 0; i < availableLanguages.Length; i++) {

			}
			catalogsUnpacked = true;
		}
	#endif
*/
	public static string _(string text)
	{
		if (_Catalog == null) return text;
		return _Catalog.GetString(text);
	}

	public static string _(string text, params object[] args)
	{
		if (_Catalog == null) return text;
		return _Catalog.GetString(text, args);
	}

	public static string _n(string text, string pluralText, long n)
	{
		if (_Catalog == null) return text;
		return _Catalog.GetPluralString(text, pluralText, n);
	}

	public static string _n(string text, string pluralText, long n, params object[] args)
	{
		if (_Catalog == null) return text;
		return _Catalog.GetPluralString(text, pluralText, n, args);
	}

	public static string _p(string context, string text)
	{
		if (_Catalog == null) return text;
		return _Catalog.GetParticularString(context, text);
	}

	public static string _p(string context, string text, params object[] args)
	{
		if (_Catalog == null) return text;
		return _Catalog.GetParticularString(context, text, args);
	}

	public static string _pn(string context, string text, string pluralText, long n)
	{
		if (_Catalog == null) return text;
		return _Catalog.GetParticularPluralString(context, text, pluralText, n);
	}

	public static string _pn(string context, string text, string pluralText, long n, params object[] args)
	{
		if (_Catalog == null) return text;
		return _Catalog.GetParticularPluralString(context, text, pluralText, n, args);
	}
}
