﻿using UnityEngine;
using System.Collections;

public class TranslatableUIText : UnityEngine.UI.Text {
	private string originalLanguage = null;
	private string originalText = null;
	private string originalTranslation = null;

	new void Start () {
		base.Start();
		if (!Application.isPlaying) return;	//make sure translation is only executed at runtime
		originalText = text;
		UpdateTranslation();
	}

	new void OnEnable () {	//on onenable check if either the target language or the field text has changes and update the translation accordingly
		base.OnEnable();
		if (!Application.isPlaying) return;	//make sure translation is only executed at runtime
		if (originalText == null) return;	//make sure OnEnable text change check doesn't happen before Start initial translation

		if (text != originalTranslation) {		//if textfield has been changed, translate new text
			originalText = text;
			UpdateTranslation();

		}
		else if (originalLanguage != T.curLanguage) {	//if target language has changed simply retranslate
			UpdateTranslation();
		}
	}

	private void UpdateTranslation () {
		text = T._(originalText);
		originalTranslation = text;
		originalLanguage = T.curLanguage;
	}

	public void ForceTranslate () {			//this function forces translation of the current string displayed. This WILL break the translation if it's called without explicitly changing the text field before
		//Debug.Log("ForceTranslate");
		if (!Application.isPlaying) return;	//make sure translation is only executed at runtime
		originalText = text;
		UpdateTranslation();
	}

	public void ForceUpdateTranslation () {	//this function forces updating the translation of the current string, with soft check for text change.
		if (!Application.isPlaying) return;	//make sure translation is only executed at runtime
		if (text != originalTranslation) {		//if textfield has been changed, translate new text
			originalText = text;
		}
		UpdateTranslation();
	}
}
